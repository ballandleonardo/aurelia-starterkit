'use strict';

// all gulp tasks are located in the ./build/tasks directory
// gulp configuration is in files in ./build directory
require('require-dir')('build/tasks');

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename');

var sassdir = './src/assets/sass/',
    cssdir = './src/assets/css/';

gulp.task('sass', function () {
  return gulp.src(sassdir + 'main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({basename: 'styles'}))
    .pipe(gulp.dest(cssdir));
});

gulp.task('sass:watch', function () {
  gulp.watch(sassdir + '/**/*.scss', ['sass']);
});

gulp.task('default', function () {
  gulp.start('sass:watch');
});

